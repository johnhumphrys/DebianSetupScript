#!/bin/bash

set -e # Exit on any error

if [ "$USER" != root ]
then
    echo "Run this script as root"
	exit 1
fi

echo "Running John's Debian Setup Script!"
sleep 3s

echo "Creating folders and moving to ~/temp"
sleep 3s
mkdir /home/john/Applications/
mkdir /home/john/Work/
mkdir /home/john/temp

cp preferences /home/john/temp
cp sources /home/john/temp

cd /home/john/temp

echo "Update"
sleep 3s
apt update

echo "Upgrade"
sleep 3s
apt upgrade

echo "Dist upgrade"
sleep 3s
apt dist-upgrade

echo "Install basic xorg"
sleep 3s
apt install gdm3 xserver-xorg xserver-xorg-core xfonts-base xinit libgl1-mesa-dri x11-xserver-utils budgie-core gir1.2-budgie-desktop gnome-session-bin gnome-session-common gnome-settings-daemon gnome-icon-theme gnome-menus gnome-terminal network-manager-gnome nautilus file-roller evince eog gedit pulseaudio alacarte gnome-control-center gnome-system-monitor

echo "Install basic network packages"
sleep 3s
apt install curl wget apt-transport-https dirmngr

echo "Install keys"
sleep 2s
apt-key adv --keyserver ha.pool.sks-keyservers.net --recv-keys EAC0D406E5D79A82ADEEDFDFB76E53652D87398A
sleep 2s
curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg
sleep 2s
sudo install -o root -g root -m 644 microsoft.gpg /etc/apt/trusted.gpg.d/
sleep 2s
sudo sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'
sleep 2s
curl -s https://updates.signal.org/desktop/apt/keys.asc | sudo apt-key add -
sleep 2s
echo "deb [arch=amd64] https://updates.signal.org/desktop/apt xenial main" | sudo tee -a /etc/apt/sources.list.d/signal-xenial.list
sleep 2s
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 931FF8E79F0876134EDDBDCCA87FF9DF48BF1C90
sleep 2s
echo deb http://repository.spotify.com stable non-free | sudo tee /etc/apt/sources.list.d/spotify.list
sleep 2s
curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
sleep 2s
curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
sleep 2s
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sleep 2s
echo deb http://repository.spotify.com stable non-free | sudo tee /etc/apt/sources.list.d/spotify.list
sleep 2s
echo deb http://ppa.launchpad.net/tista/adapta/ubuntu bionic main | sudo tee /etc/apt/sources.list.d/adapta.list
sleep 2s

echo "Update sources.list"
sleep 3s
cp preferences /etc/apt/preferences
cp sources /etc/apt/sources.list

echo "Update software"
sleep 3s
apt update

echo "Upgrade software"
sleep 3s
apt upgrade

echo "Install software"
sleep 3s
apt install yarn vrms code firefox-esr adapta-gtk-theme adapta-backgrounds breeze-cursor-theme fonts-noto fonts-hack-ttf fonts-roboto papirus-icon-theme keepassxc libreoffice libreoffice-style-* audacity thunderbird rsync synaptic geany filezilla gimp vlc remmina qbittorrent links macchanger openvpn gnupg vim git openjdk-11-jdk torbrowser-launcher p7zip htop gparted neofetch python feh bb cowsay handbrake gufw wireshark zenmap gdebi chromium trash-cli openssh-server unzip build-essential

git config --global user.name "John Humphrys"
git config --global user.email john.humphrys@pm.me

echo "Install other software"
sleep 3s

apt install -y nodejs
apt install budgie-*
apt install spotify-client signal-desktop

echo "Install other software"
sleep 3s
wget https://downloads.slack-edge.com/linux_releases/slack-desktop-3.4.2-amd64.deb
dpkg -i slack-desktop-3.3.4-amd64.deb
apt -f install

sleep 3s
cd /home/john/Applications/

sleep 3s
wget https://download.jetbrains.com/python/pycharm-community-2019.1.3.tar.gz
tar zxvf pycharm-community-2019.1.3.tar.gz
mv pycharm-community-2019.1.3 pycharm
./pycharm/bin/pycharm.sh

sleep 3s
wget https://download-cf.jetbrains.com/idea/ideaIC-2019.1.3.tar.gz
tar zxvf ideaIC-2019.1.3.tar.gz
mv idea-IC-191.7479.19 idea
./idea/bin/install.sh

sleep 3s
wget https://dl.google.com/dl/android/studio/ide-zips/3.4.1.0/android-studio-ide-183.5522156-linux.tar.gz
tar zxvf android-studio-ide-183.5522156-linux.tar.gz
./android-studio/bin/studio.sh

sleep 3s
wget https://www.arduino.cc/download.php?f=/arduino-1.8.9-linux64.tar.xz
mv 'download.php?f=%2Farduino-1.8.9-linux64.tar.xz' arduino.tar.xz
tar fxv ardunio.tar.xz
sudo ./arduino-1.8.9/install.sh

wget https://download.springsource.com/release/STS4/4.2.2.RELEASE/dist/e4.11/spring-tool-suite-4-4.2.2.RELEASE-e4.11.0-linux.gtk.x86_64.tar.gz
tar zxvf spring-tool-suite-4-4.2.2.RELEASE-e4.11.0-linux.gtk.x86_64.tar.gz


echo "Done!"
